import java.io.*;
import java.util.*;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * Created by Alexander Vodila on 8. 2. 2017.
 * For BSC
 */
public class Main {
    public static List<baseDataType> list = Collections.synchronizedList(new ArrayList<baseDataType>());
    public static class baseDataType{
        private String name;
        private int Value;

        public baseDataType(String line) {
            this.name = line.substring(0,3).toUpperCase();
            this.Value = Integer.parseInt(line.substring(4));
        }

        public baseDataType(String name, int value) {
            this.name = name;
            Value = value;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public int getValue() {
            return Value;
        }

        public void setValue(int value) {
            Value = value;
        }
    }

    public static class CustomComparator implements Comparator<baseDataType> {
        @Override
        public int compare(baseDataType o1, baseDataType o2) {
            return o1.getName().compareTo(o2.getName());
        }
    }


    static Runnable periodicTask = new Runnable() {
        public void run() {
            list=getEZviewfix(list);
            finalprint(list);
        }
    };

    public static void main(String[] args) {

        int isFile=1;
        ScheduledExecutorService executor =
                Executors.newSingleThreadScheduledExecutor();
        executor.scheduleAtFixedRate(periodicTask, 0, 60, TimeUnit.SECONDS);
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        String fileasinput=null;
        try {
            fileasinput = br.readLine();
            File f = new File(fileasinput);
            if(f.exists() && !f.isDirectory() && f.canRead()) {
                isFile=2;
            }else {
                list.add(new baseDataType(fileasinput));
            }
        } catch (IOException e) {
            System.out.println("Cannot read input");
        }
        InputStream is = null;
        for (int i=isFile;i!=0;i--) {
            try {
                if (i==2)
                    is=new FileInputStream(fileasinput);
                else
                    is = System.in;
                br = new BufferedReader(new InputStreamReader(is));

                String line = null;

                while ((line = br.readLine()) != null) {
                    if (line.equalsIgnoreCase("quit")) {
                        executor.shutdown();
                        break;
                    }
                    try {
                        list.add(new baseDataType(line));
                        list = getEZviewfix(list);
                    } catch (StringIndexOutOfBoundsException sio) {
                        System.out.println("Bad input format" + sio);
                    } catch (NumberFormatException nfi){
                        System.out.println("Bad input format" + nfi);
                    }
                }
            } catch (IOException ioe) {
                System.out.println("Exception while reading input " + ioe);
            } finally {
                try {
                    if (br != null) {
                        br.close();
                    }
                } catch (IOException ioe) {
                    System.out.println("Error while closing stream: " + ioe);
                }

            }
        }
    }

    public static void finalprint(List<baseDataType> sort) {
        for (baseDataType i: sort)
            if (i.getValue()!=0)
                System.out.println(i.getName() + " " + i.getValue());
    }
    public static List<baseDataType> sortbyname(List<baseDataType> unsort){
        Collections.sort(unsort, new CustomComparator());
        return unsort;
    }

    public static List<baseDataType> getEZviewfix(List<baseDataType> sort){         //No sort need
        List<baseDataType> finallist = new ArrayList<baseDataType>();
        boolean found = false;
        ListIterator<baseDataType> itr=sort.listIterator();
        while (itr.hasNext()) {
            baseDataType actual = (baseDataType) itr.next();
            for (baseDataType data : finallist){
                if (actual.getName().equals(data.getName())) {
                    data.setValue(data.getValue()+actual.getValue());
                    found= true;
                }
            }
            if (!found) {
                finallist.add(new baseDataType(actual.getName(), actual.getValue()));
                found = false;
            }
        }
        return finallist;
    }
}