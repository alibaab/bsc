import org.junit.jupiter.api.Test;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertNotEquals;

/**
 * Created by Alexander Vodila on 8. 2. 2017.
 */
public class MainTest {
    @Test
    public void TESTgetEZviewfix()
    {
        List<Main.baseDataType> testlist = new ArrayList<Main.baseDataType>();
        List<Main.baseDataType> testlist2 = new ArrayList<Main.baseDataType>();
        testlist.add(new Main.baseDataType("USD 1000"));
        testlist.add(new Main.baseDataType("HKD 100"));
        testlist.add(new Main.baseDataType("USD -100"));
        testlist.add(new Main.baseDataType("RMB 2000"));
        testlist.add(new Main.baseDataType("HKD 200"));
        testlist2=Main.getEZviewfix(testlist);
        Main.baseDataType lastdata=null;
        for (Main.baseDataType data : testlist2){
            assertNotEquals(lastdata,data);
            lastdata=data;
        }
    }
}


